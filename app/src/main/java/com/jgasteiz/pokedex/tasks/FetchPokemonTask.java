package com.jgasteiz.pokedex.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.jgasteiz.pokedex.R;
import com.jgasteiz.pokedex.interfaces.OnPokemonFetched;
import com.jgasteiz.pokedex.models.Pokemon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FetchPokemonTask extends AsyncTask<ArrayList<String>, Void, Void> {

    private static final String LOG_TAG = FetchPokemonTask.class.getSimpleName();
    private OnPokemonFetched mListener;
    private static final String POKEDEX_API_URL = "http://pokeapi.co/api/v2/pokemon/";

    private JSONParser jsonParser = new JSONParser();

    public FetchPokemonTask(OnPokemonFetched listener) {
        mListener = listener;
    }

    @Override
    protected Void doInBackground(ArrayList<String>... params) {

        ArrayList<String> pokemonId = params[0];

        Log.e(LOG_TAG, "Fetching a pokemon");

        JSONObject json = jsonParser.makeHttpRequest(POKEDEX_API_URL + pokemonId.get(0), "GET", new HashMap<String, String>());
        try {
            Log.e(LOG_TAG, "Pokemon fetched");
            Pokemon pokemon = new Pokemon(
                (int) json.get("id"),
                (String) json.get("name")
            );

            mListener.onPokemonFetched(pokemon);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Bitmap getImageBitmap(String src) {
        try {
            Log.e("src", src);
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Log.e("Bitmap","returned");
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Exception",e.getMessage());
            return null;
        }
    }
}
