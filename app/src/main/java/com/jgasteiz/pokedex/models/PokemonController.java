package com.jgasteiz.pokedex.models;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;

import com.jgasteiz.pokedex.R;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;

/**
 * Created by javi on 30/07/16.
 */
public class PokemonController {

    public static ArrayList<Pokemon> getPokemon (Resources resources) {
        ArrayList<Pokemon> pokemonList = new ArrayList<Pokemon>();

        try {
            XmlResourceParser xml = resources.getXml(R.xml.pokemon);
            int eventType = xml.getEventType();

            Pokemon pokemon = new Pokemon();
            String currentTag = "";

            while (eventType != XmlResourceParser.END_DOCUMENT) {
                if(eventType == XmlResourceParser.START_DOCUMENT) {
                } else if(eventType == XmlResourceParser.START_TAG) {
                    if (Objects.equals(xml.getName(), "pokemon")) {
                        pokemon = new Pokemon();
                    } else {
                        currentTag = xml.getName();
                    }
                } else if(eventType == XmlResourceParser.END_TAG) {
                    if (Objects.equals(xml.getName(), "pokemon")) {
                        pokemonList.add(0, pokemon);
                    }
                } else if(eventType == XmlResourceParser.TEXT) {
                    if (Objects.equals(currentTag, "index")) {
                        pokemon.setIndex(Integer.parseInt(xml.getText()));
                    } else if (Objects.equals(currentTag, "name")) {
                        pokemon.setName(xml.getText());
                    } else if (Objects.equals(currentTag, "image_name")) {
                        pokemon.setImage(xml.getText());
                    } else if (Objects.equals(currentTag, "description")) {
                        pokemon.setImage(xml.getText());
                    }
                }
                eventType = xml.next();
            }
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }

        Collections.sort(pokemonList, new Comparator<Pokemon>() {
            @Override
            public int compare(Pokemon p1, Pokemon p2) {
                return new BigDecimal(p1.getIndex()).compareTo(new BigDecimal(p2.getIndex()));
            }
        });

        return pokemonList;
    }

    public static Pokemon getPokemonByIndex(int index, Resources resources) {
        Pokemon result = new Pokemon();

        try {
            XmlResourceParser xml = resources.getXml(R.xml.pokemon);
            int eventType = xml.getEventType();

            Pokemon pokemon = new Pokemon();
            String currentTag = "";

            while (eventType != XmlResourceParser.END_DOCUMENT) {
                if(eventType == XmlResourceParser.START_DOCUMENT) {
                } else if(eventType == XmlResourceParser.START_TAG) {
                    if (Objects.equals(xml.getName(), "pokemon")) {
                        pokemon = new Pokemon();
                    } else {
                        currentTag = xml.getName();
                    }
                } else if(eventType == XmlResourceParser.END_TAG) {
                    if (Objects.equals(xml.getName(), "pokemon")) {
                        if (pokemon.getIndex() == index) {
                            result = pokemon;
                            break;
                        }
                    }
                } else if(eventType == XmlResourceParser.TEXT) {
                    if (Objects.equals(currentTag, "index")) {
                        pokemon.setIndex(Integer.parseInt(xml.getText()));
                    } else if (Objects.equals(currentTag, "name")) {
                        pokemon.setName(xml.getText());
                    } else if (Objects.equals(currentTag, "image_name")) {
                        pokemon.setImage(xml.getText());
                    } else if (Objects.equals(currentTag, "description")) {
                        pokemon.setDescription(xml.getText());
                    }
                }
                eventType = xml.next();
            }
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
