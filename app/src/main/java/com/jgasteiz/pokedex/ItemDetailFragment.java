package com.jgasteiz.pokedex;

import android.app.Activity;
import android.media.Image;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.jgasteiz.pokedex.interfaces.OnPokemonFetched;
import com.jgasteiz.pokedex.models.Pokemon;
import com.jgasteiz.pokedex.models.PokemonController;
import com.jgasteiz.pokedex.tasks.FetchPokemonTask;

import java.util.ArrayList;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String POKEMON_INDEX = "pokemon_index";

    private static final String LOG_TAG = ItemDetailFragment.class.getSimpleName();

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);

        int pokemonIndex = getArguments().getInt(POKEMON_INDEX);

        Pokemon pokemon = PokemonController.getPokemonByIndex(pokemonIndex, getResources());

        // Set image and name
        int identifier = getResources().getIdentifier(pokemon.getImage(), "drawable", getActivity().getPackageName());
        ((ImageView) rootView.findViewById(R.id.item_image)).setImageDrawable(ContextCompat.getDrawable(getActivity(), identifier));
        ((TextView) rootView.findViewById(R.id.item_name)).setText(pokemon.getName());
        ((TextView) rootView.findViewById(R.id.item_description)).setText(pokemon.getDescription());

        return rootView;
    }
}
