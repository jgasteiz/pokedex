package com.jgasteiz.pokedex.interfaces;

import com.jgasteiz.pokedex.models.Pokemon;

import java.util.List;

public interface OnPokemonListFetched {
    void onPokemonListFetched(List<Pokemon> pokemonList);
}
