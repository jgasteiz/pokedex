import os
import requests
import scrapy

DETAIL_URL_SELECTOR = 'a[data-sprite*="pkgG1"]::attr("href")'
IMG_SRC_SELECTOR = '.svtabs-panel:first-child .figure img::attr("src")'


class SpriteSpider(scrapy.Spider):
    """
    Scrapes the 1st generation (151) Pokemon sprites
    """
    name = 'sprites'
    start_urls = ['http://pokemondb.net/pokedex/national']

    def parse(self, response):
        count = 0
        for url in response.css(DETAIL_URL_SELECTOR).re('.*/pokedex/.*'):
            count += 1
            yield scrapy.Request(response.urljoin(url), self.parse_sprites)

    def parse_sprites(self, response):
        for image_url in response.css(IMG_SRC_SELECTOR).extract():
            file_name = image_url.split('/')[-1]
            r = requests.get(image_url)
            with open('images/{}'.format(file_name), 'wb') as outfile:
                outfile.write(r.content)
            yield {'image_url': image_url}
