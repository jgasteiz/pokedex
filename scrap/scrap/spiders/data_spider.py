import os
import requests
import scrapy

DETAIL_URL_SELECTOR = 'a[data-sprite*="pkgG1"]::attr("href")'
DESCRIPTION_SELECTOR = ''


class DataSpider(scrapy.Spider):
    """
    Scrapes the 1st generation (151) Pokemon data
    """
    name = 'data'
    start_urls = ['http://pokemondb.net/pokedex/national']

    def parse(self, response):
        for url in response.css(DETAIL_URL_SELECTOR).re('.*/pokedex/.*'):
            yield scrapy.Request(response.urljoin(url), self.parse_data)

    def parse_data(self, response):
        index = response.css('.figure + div table tr:first-child td strong::text').extract_first()
        name = response.css('h1::text').extract_first()
        description = response.css('#dex-flavor + h2 + table tr:first-child td::text').extract_first()
        image_src = response.css('.svtabs-panel:first-child .figure img::attr("src")').extract_first()
        image_name = image_src.split('/')[-1].replace('.jpg', '')

        yield {
            'index': index,
            'name': name,
            'description': description,
            'image_name': image_name,
        }
